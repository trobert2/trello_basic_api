import logging
import os

import requests


logging.basicConfig(level=logging.INFO)
log = logging.getLogger()


class KanbanException(Exception):
    pass


class Trello(object):
    base_api_endpoint = "https://api.trello.com/1/"

    def __init__(self, api_key: str, token: str):
        self.default_headers = {'Content-Type': 'application/json'}
        self.default_params = {"key": api_key, "token": token}

    def _execute_request(self, method: str, resource_endpoint: str, additional_parameters: dict = {}) -> dict:
        complete_endpoint = os.path.join(self.base_api_endpoint, resource_endpoint)

        parameters = {}
        parameters.update(additional_parameters)
        parameters.update(self.default_params)

        # TODO: Implement PUT and DELETE
        if method.upper() == "GET":
            response = requests.get(complete_endpoint, params=parameters, headers=self.default_headers)
        elif method.upper() == "POST":
            response = requests.post(complete_endpoint, params=parameters, headers=self.default_headers)
        else:
            # TODO: Use a proper exception before submission
            raise KanbanException("Unsupported method!")

        log.debug("response code: %s", response.status_code)
        log.debug("response text: %s", response.text)
        if response.status_code == requests.codes.ok:
            json_response = response.json()
            log.debug("Request was successful: %s", json_response)
            return json_response

        raise KanbanException("Request did not succeed!")

    def list_boards(self) -> list:
        current_user_endpoint = "members/me/"

        try:
            user_details = self._execute_request("get", current_user_endpoint)
            return user_details['idBoards']
        except KanbanException as e:
            log.error(e)
            return []

    def get_board_by_id(self, board_id: str) -> dict:
        current_board_endpoint = os.path.join("boards", board_id)

        try:
            board_details = self._execute_request("get", current_board_endpoint)
            return board_details
        except KanbanException as e:
            log.error(e)
            return {}

    def list_lists(self, board_id: str) -> list:
        lists_endpoint = os.path.join("boards", board_id, "lists")

        try:
            return self._execute_request("get", lists_endpoint)
        except KanbanException as e:
            log.error(e)
            return []

    def list_cards_by_list_id(self, list_id: str) -> list:
        cards_endpoint = os.path.join("lists", list_id, "cards")

        try:
            return self._execute_request("get", cards_endpoint)
        except KanbanException as e:
            log.error(e)
            return []

    def create_card(self, list_id: str, card_name: str) -> dict:
        cards_endpoint = os.path.join("lists", list_id, "cards")

        params = {
            "name": card_name,
            "idList": list_id
        }

        try:
            response = self._execute_request("post", cards_endpoint, params)
            return response
        except KanbanException as e:
            log.error(e)
            return {}

    def add_card_comment(self, card_id: str, comment: str) -> dict:
        card_comment_endpoint = os.path.join("cards", card_id, "actions", "comments")

        params = {
            "text": comment,
        }

        try:
            response = self._execute_request("post", card_comment_endpoint, params)
            return response
        except KanbanException as e:
            log.error(e)
            return {}

    def add_card_label(self, card_id: str, label: str) -> dict:
        card_label_endpoint = os.path.join("cards", card_id, "labels")

        params = {
            "color": label,
        }

        try:
            response = self._execute_request("post", card_label_endpoint, params)
            return response
        except KanbanException as e:
            log.error(e)
            return {}
