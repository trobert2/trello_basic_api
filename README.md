# Overview

This project contains the library neede for the solution to the "Take home test" which requires the creation of a Trello card.

# Proposed solution

## Description
The program shall be implemented as a CLI which will allow us to re-use and extend as needed. The library hosted here can be imported and used in resolving the problem

## Design points
* The project will be written using `python3`.

* The project shall be *pep8* compliant and this shall be tested using `flake8`.

* Unit tests shall be added.

* Since it is not specified in the requirements, we assume the user has been already authenticated so the implemented class will ask for an *api key* and an *authentication token*.
> NOTE: In a real life scenario the authorisation and authentication mechanisms would have been implemented as well.
> Documentation on the subject can be found [here](https://developers.trello.com/page/authorization)

* For an easier and cleaner distribution this project will be `Dockerised`.

* Since the use of the main SDK is forbidden, the implementation will be done using the `requests` library provided. We will call the rest api [documented here](https://developers.trello.com/reference/).

# Building

```
docker build -t trello_basic_api .
```

# Running the tests
The tests are ran using `pytest` during the docker build, along with the `flake8`
