import os

import click

from trello_basic_api.trello import Trello


class NoCredentialsException(Exception):
    pass


def _get_credentials() -> dict:
    api_key = os.getenv("TRELLO_API_KEY", "")
    token = os.getenv("TRELLO_TOKEN", "")

    if len(token) == 0 or len(api_key) == 0:
        raise NoCredentialsException('Please provide proper values for TRELLO_API_KEY (got "%s") and TRELLO_TOKEN (got "%s")' % (api_key, token))

    return {"trello_api_key": api_key, "trello_token": token}


def _display_choices_and_select(elements: list) -> int:
    name_col_width = max(len(element['name']) for element in elements) + 2  # padding
    id_col_width = max(len(element['name']) for element in elements) + 2  # padding

    if len(elements) == 1:
        click.echo("Only one element can be selected")
        return elements[0]
    else:
        index = 0
        click.echo("%s  %s %s" % ("Index:".ljust(7), "Name:".ljust(name_col_width), "Id:".ljust(id_col_width)))
        for element in elements:
            index_str = "[%s]" % str(index)
            click.echo("%s  %s %s" % (index_str.ljust(7), element['name'].ljust(name_col_width), element['id'].ljust(id_col_width)))
            index += 1
        selected_index = int(input("Choice: "))
        return elements[selected_index]


@click.group()
@click.pass_context
def cli(ctx):
    ctx.ensure_object(dict)


@cli.command()
@click.argument('name')
@click.pass_context
def create_card(ctx, name: str):
    api_object = ctx.obj['trello_client']

    # Fetch the existing boards for the current user. REST api provides only IDs here.
    board_ids = api_object.list_boards()

    # Fetch the the board details for all of the existing boards
    boards = []
    for board_id in board_ids:
        board = {"id": board_id}

        board['name'] = api_object.get_board_by_id(board_id)['name']

        boards.append(board)

    selected_board = _display_choices_and_select(boards)

    # Fetch existing lists in the selected board
    # TODO: This section is repeated. Export to a function to avoid duplications.
    lists = api_object.list_lists(selected_board['id'])

    selected_list = _display_choices_and_select(lists)

    api_object.create_card(selected_list['id'], name)
    click.echo("Board Created With Success!")


@cli.command()
@click.argument('comment')
@click.pass_context
def add_comment(ctx, comment: str):
    api_object = ctx.obj['trello_client']

    # Fetch the existing boards for the current user. REST api provides only IDs here.
    board_ids = api_object.list_boards()

    # Fetch the the board details for all of the existing boards
    boards = []
    for board_id in board_ids:
        board = {"id": board_id}

        board['name'] = api_object.get_board_by_id(board_id)['name']

        boards.append(board)

    selected_board = _display_choices_and_select(boards)

    # Fetch existing lists in the selected board
    lists = api_object.list_lists(selected_board['id'])

    selected_list = _display_choices_and_select(lists)

    # Fetch existing cards in the selected board
    cards = api_object.list_cards_by_list_id(selected_list['id'])

    selected_card = _display_choices_and_select(cards)

    api_object.add_card_comment(selected_card['id'], comment)
    click.echo("Comment Added With Success!")


@cli.command()
@click.argument('labels')
@click.pass_context
def add_labels(ctx, labels: str):
    api_object = ctx.obj['trello_client']

    # Fetch the existing boards for the current user. REST api provides only IDs here.
    board_ids = api_object.list_boards()

    # Fetch the the board details for all of the existing boards
    boards = []
    for board_id in board_ids:
        board = {"id": board_id}

        board['name'] = api_object.get_board_by_id(board_id)['name']

        boards.append(board)

    selected_board = _display_choices_and_select(boards)

    # Fetch existing lists in the selected board
    lists = api_object.list_lists(selected_board['id'])

    selected_list = _display_choices_and_select(lists)

    # Fetch existing cards in the selected board
    cards = api_object.list_cards_by_list_id(selected_list['id'])

    selected_card = _display_choices_and_select(cards)

    labels_list = labels.split(",")
    for label in labels_list:
        api_object.add_card_label(selected_card['id'], label)
    click.echo("Lables Added With Success!")


def main():
    credentials = _get_credentials()
    trello_client = Trello(api_key=credentials['trello_api_key'], token=credentials['trello_token'])
    cli(obj={'trello_client': trello_client})


if __name__ == '__main__':
    main()
