#!/usr/bin/env python3

from setuptools import setup
import os

here = os.path.abspath(os.path.dirname(__file__))


def read_readme():
    with open(os.path.join(here, 'README.md')) as f:
        return f.read()


def get_requirements():
    with open(os.path.join(here, 'requirements.txt')) as f:
        return f.readlines()


setup(
    name="trello_basic_api",
    version='0.0.1',
    description='CLI tool that allows creation of boards, cards and further interation with Kanban board providers.',
    long_description=read_readme(),
    url='https://gitlab.com/take-home-test/trello_basic_api',
    author='Robert Tingirică',
    license='Apache 2.0',
    install_requires=get_requirements(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'trello_basic_api = trello_basic_api.run:main',
        ]
    }
)
