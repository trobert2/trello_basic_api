FROM python:3.7-alpine

COPY . /trello_basic_api

ENV PYTHONPATH /
WORKDIR /trello_basic_api

RUN apk add --no-cache ca-certificates python3-dev
RUN python3 setup.py install && \
    flake8 .
